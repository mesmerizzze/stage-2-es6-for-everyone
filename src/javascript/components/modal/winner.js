import { showModal } from './modal'
import { createElement } from '../../helpers/domHelper'
import { createFighterImage } from '../fighterPreview'

export function showWinnerModal(fighter) {
  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  const winnerImg = createFighterImage(fighter);

  bodyElement.append(winnerImg);

  showModal({ title: `${fighter.name} is a winner!`, bodyElement });
}
